#!/usr/bin/env python3

"""
Check and test the bootc-group-access.yml file conforms with expectations

This file is automatically called when python's unittests are run.  It may
also be executed manually on the command-line, assuming the (very common)
PyYAML requirement is met.
"""

import unittest

from yaml import safe_load as yaml_load


class BootcGroupAccess(unittest.TestCase):

    ACCESS_FILE = "bootc-group-access.yml"
    bga = None  # assigned during setUp()

    def setUp(self):
        with open(self.ACCESS_FILE) as access_file:
            self.bga = yaml_load(access_file)

    # Tests execute in alphabetical order, we want this one 1st
    def test_00_can_load(self):
        self.assertIsNotNone(self.bga)

    # Tests execute in alphabetical order, we want this one 2nd
    def test_01_is_map(self):
        self.assertIsInstance(self.bga, dict)

    # Tests execute in alphabetical order, we want this one 3rd
    def test_02_not_empty(self):
        self.assertGreater(len(self.bga.keys()), 0)

    # Tests execute in alphabetical order, we want this one 4th
    def test_03_is_sorted(self):
        # PyYaml is suppose to preserve dict key order, but bugs have happened
        test_yaml = """
---
beta: 2
alpha: 1
"""
        dct = yaml_load(test_yaml)
        actual = list(dct.keys())
        expected = ["beta", "alpha"]
        self.assertEqual(
            actual, expected, "Bug: PyYAML safe_load is sorting map keys, it must not."
        )

        actual = list(self.bga.keys())
        expected = list(self.bga.keys())
        expected.sort()
        self.assertEqual(
            actual,
            expected,
            f"Map keys in {self.ACCESS_FILE} not in alphabetical order.",
        )


if __name__ == "__main__":
    unittest.main()
