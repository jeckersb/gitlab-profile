# Welcome to the bootc-Org

## Description

This gitlab group contains bootc project repositories.

## Membership

Being a public group, anybody is welcome to participate using the standard
*Fork + Merge Request* contribution model.  If access beyond general contributor
level is desired, a request may be submitted using the following process:

1. [Fork this gitlab-profile repository](https://gitlab.com/bootc-org/gitlab-profile/-/forks/new).
1. Clone, and create a new local working branch.
1. Open the `bootc-group-access.yml` file in your favorite editor.
1. Following the existing format, add *in alphabetical order*, your gitlab.com username name
   and desired access-level.  The minimum access-level request is `developer`.
   **WARNING**: Do not request `owner` level access, it will be rejected.
1. Save the file, create a git commit with the change, and push to your fork.
1. [Open a MergeRequest to (this) `gitlab-profile`
   repository](https://gitlab.com/bootc-org/gitlab-profile/-/merge_requests/new). If
   requesting an access level higher than `reporter`, please specify the justification/reason
   in the MR description field.
1. Assign your merge request to:
   - @cevich
   - @lmilbaum

All requests for access should be handled within 24 business hours.  If several days
pass without comment or action, please ping one/more of [the *owners* listed on
the membership page](https://gitlab.com/bootc-org/gitlab-profile/-/project_members).
